var unique=0

var traverseDomAndCollectElements = function(matchFunc,startEl=document.body) {
  unique++
  var resultSet = [];
  matchFunc(startEl) && resultSet.push(startEl)
  startEl.search=startEl.search?startEl.search:unique
  if (startEl.children.length!=0){
    for (let i = 0; i < startEl.children.length; i++) {
      resultSet=resultSet.concat(traverseDomAndCollectElements(matchFunc,startEl.children[i]))
    }
  }
  return resultSet;
};

var lookElementsAndCollect=function(matchDesc,matchElements,currentElement, elements){
  var resultSet=[]
  if (elements==undefined){
    matchDesc(currentElement) && resultSet.push(currentElement)
    for (let i = 0; i < currentElement.children.length; i++) {
      if(!matchElements(currentElement.children[i])){
        resultSet=resultSet.concat(lookElementsAndCollect(matchDesc,matchElements,currentElement.children[i]))
      }
    }
  }else{
    for (let i = 0; i < elements.length; i++) {
      resultSet=resultSet.concat(lookElementsAndCollect(matchDesc,matchElements,elements[i]))
    }
  }
  return resultSet
}

var selectorTypeMatcher = function(selector) {
  var patt1 = new RegExp(/[.#>]/);
  var patt5 = new RegExp(/[>]/);
  switch (selector[0]){
    case '.':
    return 'class'
    case '#':
    return 'id'
  }
  if(patt1.test(selector)){
    if (patt5.test(selector)){
      selector=selector.split('>').map((e)=>e.trim()).length
      if (selector==2){
        return 'child'
      }
    }
    if(selector[0]!='.' && selector[0]!='#'){
      if(selector.split('.').length==2){
        return 'tag.class'
      } return 'tag'
    }
  }else{
    if (selector.split(' ').length==2){
      return 'descendant'
    }else return "tag"
  }
};


var matchFunctionMaker = function(selector) {
  var selectorType = selectorTypeMatcher(selector);
  var matchFunction
  if(['child','tag.class',"descendant"].indexOf(selectorType)!=-1){
    var first=selector.split(selectorType=='child'?('>'):(selectorType=='tag.class'?'.':' '))[0].trim()
    var second=selector.split(selectorType=='child'?('>'):(selectorType=='tag.class'?'.':' '))[1].trim()
  }
  if(['id','class','tag','child','tag.class'].indexOf(selectorType)!=-1){
    matchFunction=function(element){
      switch (selectorType){
        case 'id':
          condition= element.id==selector.substr(1)?true:false
          break
        case 'class':
          condition= element.className.split(' ').indexOf(selector.substr(1))!=-1?true:false
          break
        case 'tag':
        condition= element.tagName==selector.toUpperCase()?true:false
          break
        case 'child':
          condition=element.tagName==second.toUpperCase() && element.parentElement.tagName==first.toUpperCase()
          break
        case 'tag.class':
          condition=element.tagName==first.toUpperCase() && element.className.split(' ').indexOf(second)!=-1
          break
              }
      return condition?true:false
      }
  }else if (selectorType === "descendant") {
        function find(element,compare){
          return (compare(element))?true: find(element.parentElement,compare)
        }
        matchFunction=function(element){
          var compare1=matchFunctionMaker(first)
          var compare2=matchFunctionMaker(second)
          return compare2(element)?find(element.parentElement,compare1):false
        }
      } 
  return matchFunction;
};

var $ = function(selector) {
  
 selector=selector.trim().split(' ').map(e=>e.trim())
  var elements;
  var selectorMatchFunc
  if(selector.length==3 && selector.indexOf('>')!=-1){
    selector=selector.join(' ')
  selectorMatchFunc = matchFunctionMaker(selector);
  elements = traverseDomAndCollectElements(selectorMatchFunc);
  }else if (selector.length>2){
    for (let i = 0; i < selector.length-1; i++) {
      if(elements==undefined){
        
        elements=selector[i+1]=='>'?
        ($(`${selector[i]} > ${selector[++i+1]}`)):
        ($(`${selector[i]} ${selector[i+1]}`))
       
      }else if(selector[i+1]=='>'){
        i+=1
        
        elements=elements.children(`${selector[i+1]}`)
      }else if(selector[i+1]!='>'){
        
        elements=elements.find(`${selector[i+1]}`)
      }
    }
}else{
  selector=selector.join(' ')
  selectorMatchFunc = matchFunctionMaker(selector);
  elements = traverseDomAndCollectElements(selectorMatchFunc);
 }
return convertArray(elements)
};

function convertArray(array){
  array.indexArray=array.map(e=>e.search)
  array.children=function(childSelector){
    var match=matchFunctionMaker(childSelector)
    var nuevo= array.map(e=>{
      var list=[]
      for (let i = 0; i < e.children.length; i++) {
        match(e.children[i]) && list.push(e.children[i])
      }
      return list
    }).reduce((total,e)=>total=total.concat(e))
    return convertArray(nuevo)
  }
  array.find=function(childSelector){
    var matchOther=matchFunctionMaker(childSelector)
    var matchThis=function(element){
      return array.indexArray.indexOf(element.search)!=-1
    }
    var nuevo=lookElementsAndCollect(matchOther,matchThis,null,array)
    return convertArray(nuevo)
  }
  array.get=function(i){
    return this[i]
  }
  return array
}
